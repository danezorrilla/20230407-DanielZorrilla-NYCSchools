package com.example.a20220108_danielzorrilla_nycschools.View

import org.junit.Assert.*
import org.junit.Test

class UserRegTest{

    @Test
    fun `empty username returns false`(){
        // Pass the value to the function of RegistrationUtil class
        // since RegistrationUtil is an object/ singleton we do not need to create its object
        val result = UserReg.validRegistrationInput(
            "",
            "123",
            "123"
        )
        // assertThat() comes from the truth library that we added earlier
        // put result in it and assign the boolean that it should return
        assertFalse(result)
    }
}